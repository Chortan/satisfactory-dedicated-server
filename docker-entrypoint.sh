#!/bin/sh

if [ -z "$CRASH_UPLOAD" ]; then
    CRASH_UPLOAD=true
else
    CRASH_UPLOAD=false
fi


echo -e "\n[CrashReportClient]\nbAgreeToCrashUpload=$CRASH_UPLOAD" > FactoryGame/Saved/Config/LinuxServer/Engine.ini

echo "Run Satisfactory Dedicated Server"

./FactoryServer.sh
