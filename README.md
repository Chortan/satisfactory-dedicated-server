# Satisfactory Dedicated Server

This is a short guide on how to set up a [Satisfactory](https://satisfactory.fandom.com/wiki/Satisfactory) dedicated server on docker. 

## Requirements & Port

The server needs a minimum of **5 GB RAM** to run. It is recommended to run at **8 GB RAM** to support 4 players. 

|Default Port (UDP Only)| Name        |Note                                                                                               |
|:----------------------|:------------|:--------------------------------------------------------------------------------------------------|
| 15777/udp             | Query Port  |This is the port that you need to enter in the game when you first connect to a dedicated server.  |
| 15000/udp             | Beacon Port |                                                                                                   |
| 7777/udp              | Game Port   |                                                                                                   |

## Usage

### Build

```shell
docker build -t satisfactory .
```

### Run

Docker command

```shell
mkdir -p /opt/satisfactory/save
docker run -d -p 15777:15777/udp -p 15000:15000/udp 7777:7777/udp \
   -v /opt/satisfactory/save:/home/satisfactory/.config/Epic/FactoryGame/Saved/SaveGames/server satisfactory
```

docker compose file

```yml
version: '3.8'
services:
  satisfactory:
    image: satisfactory
    ports:
      - "15777:15777/udp"
      - "7777:7777/udp"
      - "15000:15000/udp"
    environment:
      - CRASH_UPLOAD=true
    volumes:
      - /opt/satisfactory/save:/home/satisfactory/.config/Epic/FactoryGame/Saved/SaveGames/server
```

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

I want [contribue](https://gitlab.com/Chortan/satisfactory-dedicated-server/-/issues) !


## License

Distributed under the MIT License. See `LICENSE.md` for more information.


## Maintainer

Jordan Roimarmier - jordan.roimarmier@outlook.com

## Links

Project Link: [Satisfactory Dedicated Server](https://gitlab.com/Chortan/satisfactory-dedicated-server)
Satifactory Dedicated Serevr [Official Wiki](https://satisfactory.fandom.com/wiki/Dedicated_servers)
