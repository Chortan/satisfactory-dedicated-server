FROM steamcmd/steamcmd:ubuntu-20

LABEL name="Satisfactory Dedicated Server"                           \
      description="Satisfactory Dedicated Server packaged on Docker" \
      version="1.0.0"                                               \
      maintainer="Chortan"

ENV USER=satisfactory \
    HOME=/home/satisfactory


# Ubuntu
RUN adduser \
    --disabled-password \
    --gecos "" \
    --home /home/satisfactory \
    "$USER"

# Alpine
#RUN adduser -D $USER

WORKDIR $HOME

USER satisfactory

RUN /usr/bin/steamcmd +login anonymous +force_install_dir SatisfactoryDedicatedServer +app_update 1690800 +quit


WORKDIR /home/satisfactory/.steam/steamcmd/SatisfactoryDedicatedServer

ADD --chown=satisfactory:satisfactory docker-entrypoint.sh docker-entrypoint.sh

RUN chmod +x docker-entrypoint.sh && \ 
    mkdir -p /home/satisfactory/.config/Epic/FactoryGame/Saved/SaveGames/server

ENTRYPOINT ["/bin/sh"]
CMD ["./docker-entrypoint.sh"]
